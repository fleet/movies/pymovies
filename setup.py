from distutils.core import setup

setup(
    name='Movies3D python bindings',
    version='0.1.0',
    author='L.Berger',
    author_email='laurent.berger@ifremer.fr',
    packages=['pymovies'],
	package_dir={'pymovies':'.'},
	package_data={'pymovies': ['*.dll','*.xml',"*.pyd","*.csv"]},
    url='https://gitlab.ifremer.fr/fleet/movies/pymovies',
 
	license='LICENSE',
    description='Movies3D python scripts',
)